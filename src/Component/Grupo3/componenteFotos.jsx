import React, { useState } from 'react';

import AddIcon from "@material-ui/icons/Add";
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import { red } from '@material-ui/core/colors';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Fab, Button } from "@material-ui/core";
import Upload from "./Upload_Photo_Button";
import Save_Image from "./Save_Image";

import image1 from '../../Model/Grupo3/Assets/FotosDeportes/dep1.jpg'
import image2 from '../../Model/Grupo3/Assets/FotosDeportes/dep2.jpg'
import image3 from '../../Model/Grupo3/Assets/FotosDeportes/dep3.jpg'
import image4 from '../../Model/Grupo3/Assets/FotosDeportes/dep4.jpg'
import image5 from '../../Model/Grupo3/Assets/FotosDeportes/dep5.jpg'

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 400,
    },
    selectEmpty: {
        marginTop: theme.spacing(5),
    },

    root1: {
        flexGrow: 1,
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(1),
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    gridList: {
        width: 600,
        height: 450,
    },
    gridProfessor: {
        width: 450,
        height: 450,
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },

}));


export default function NativeSelects() {
    const classes = useStyles();
    const [state, setState] = React.useState({
        age: '',
        name: 'hai',
    });

    const handleChange = (event) => {
        const name = event.target.name;
        setState({
            ...state,
            [name]: event.target.value,
        });
    };

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const [file, setFile] = useState()
    const [imagenes, setImagenes] = useState([])
    const [nameFile, setNameFile] = useState('')

    const cargarImagen = (imagen) => {
        // setFile(imagen)
        var tempImagenes = [];
        for (let index = 0; index < imagen.target.files.length; index++) {
            const reader = new FileReader();
            var url = reader.readAsDataURL(imagen.target.files[index]);
            reader.onloadend = function (e) {
                // setFile(reader.result)
                tempImagenes.push({img: reader.result, title: 'Eliminar Foto'})
                setImagenes(tempImagenes)
            }
        }
    }
    const tileData = [
        {
            img: file,
            title: 'Eliminar foto',
        },
        {
            img: image2,
            title: 'Eliminar foto',
        },
        {
            img: image3,
            title: 'Eliminar foto',
        },
        {
            img: image4,
            title: 'Eliminar foto',
        },
        {
            img: image4,
            title: 'Eliminar foto',
        },
        {
            img: image4,
            title: 'Eliminar foto',
        },
    ];

    const image_professor = [
        {
            img: image5,
            title: 'Eliminar foto',
        },
    ]


    return (
        <div className={classes.root1}>
            <Grid container spacing={3}>
                <Grid item xs={2}></Grid>
                <Grid item xs={4}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="age-native-simple">Selecione</InputLabel>
                        <Select
                            native
                            onChange={handleChange}
                            inputProps={{
                                name: 'age',
                                id: 'age-native-simple',
                            }}
                        >
                            <option aria-label="None" value="" />
                            <option value={10}>Basquet</option>
                            <option value={20}>Tenis</option>
                            <option value={30}>Tenis de mesa</option>
                        </Select>
                        <FormHelperText>Seleccione Deporte</FormHelperText>
                    </FormControl>
                </Grid>
                <Grid item xs={4}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="age-native-helper">Seleccione</InputLabel>
                        <Select
                            native
                            onChange={handleChange}
                            inputProps={{
                                name: 'age',
                                id: 'age-native-helper',
                            }}
                        >
                            <option aria-label="None" value="" />
                            <option value={10}>Fernando</option>
                            <option value={20}>Jose</option>
                            <option value={30}>Marco</option>
                        </Select>
                        <FormHelperText>Seleccione su Profesor</FormHelperText>
                    </FormControl>
                </Grid>
                <Grid item xs={2}></Grid>

                <Grid item xs={2}></Grid>
                <Grid item xs={4}>
                    <div className={classes.root}>
                        <GridList cellHeight={180} className={classes.gridList}>
                            <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
                                <ListSubheader component="div">Elimine o carge las imagenes</ListSubheader>
                            </GridListTile>
                            {imagenes.map((tile) => (
                                <GridListTile key={tile.img}>
                                    <img src={tile.img} alt={tile.title} />
                                    <GridListTileBar
                                        title={tile.title}
                                        //subtitle={<span>by: {tile.author}</span>}
                                        actionIcon={
                                            <IconButton aria-label={`info about ${tile.title}`} className={classes.icon} onClick={handleClickOpen}>
                                                <DeleteIcon fontSize="small" style={{ color: red[500] }} />
                                                <Dialog
                                                    open={open}
                                                    onClose={handleClose}
                                                    aria-labelledby="alert-dialog-title"
                                                    aria-describedby="alert-dialog-description"
                                                >
                                                    <DialogTitle id="alert-dialog-title">{"Eliminar foto"}</DialogTitle>
                                                    <DialogContent>
                                                        <DialogContentText id="alert-dialog-description">
                                                            ¿Esta seguro de eliminar la foto seleccionada?
                                                        </DialogContentText>
                                                    </DialogContent>
                                                    <DialogActions>
                                                        <Button onClick={handleClose} color="primary">
                                                            Cancelar
                                                        </Button>
                                                        <Button onClick={handleClose} color="primary" autoFocus>
                                                            Aceptar
                                                        </Button>
                                                    </DialogActions>
                                                </Dialog>
                                            </IconButton>
                                        }
                                    />
                                </GridListTile>
                            ))}
                        </GridList>
                    </div>
                </Grid>
                <Grid item xs={4}>
                    <div className={classes.root}>
                        <GridList cellHeight={160} className={classes.gridList} cols={1}>
                            <GridListTile key="Subheader" style={{ height: '160' }}>
                                <ListSubheader component="div">Elimine o carge las imagenes</ListSubheader>
                            </GridListTile>
                            {image_professor.map((tile) => (
                                <GridListTile key={tile.img}>
                                    <img src={tile.img} alt={tile.title} />
                                    <GridListTileBar
                                        title={tile.title}
                                        actionIcon={
                                            <IconButton aria-label={`info about ${tile.title}`} className={classes.icon}>
                                                <DeleteIcon fontSize="small" style={{ color: red[500] }} />
                                                <Dialog
                                                    open={open}
                                                    onClose={handleClose}
                                                    aria-labelledby="alert-dialog-title"
                                                    aria-describedby="alert-dialog-description"
                                                >
                                                    <DialogTitle id="alert-dialog-title">{"Eliminar foto"}</DialogTitle>
                                                    <DialogContent>
                                                        <DialogContentText id="alert-dialog-description">
                                                            ¿Esta seguro de eliminar la foto seleccionada?
                                                        </DialogContentText>
                                                    </DialogContent>
                                                    <DialogActions>
                                                        <Button onClick={handleClose} color="primary">
                                                            Cancelar
                                                        </Button>
                                                        <Button onClick={handleClose} color="primary" autoFocus>
                                                            Aceptar
                                                        </Button>
                                                    </DialogActions>
                                                </Dialog>
                                            </IconButton>
                                        }
                                    />
                                </GridListTile>
                            ))}
                        </GridList>
                    </div>
                </Grid>
                <Grid item xs={2}></Grid>

            </Grid>
            <Grid container spacing={3}>
                <Grid item xs={2}></Grid>
                <Grid item xs={4}>
                    <Grid container spacing={1}>
                        <Grid item xs={4}>
                            <div className="App">
                                <label htmlFor="upload-photo">
                                    <input multiple="multiple"
                                        style={{ display: "none" }}
                                        id="upload-photo"
                                        name="upload-photo"
                                        type="file"
                                        onChange={e => { cargarImagen(e) }}
                                    // onChange={e => { cargarImagen(e.target.files[0]) }}
                                    />
                                    <Fab
                                        color="secondary"
                                        size="small"
                                        component="span"
                                        aria-label="add"
                                        variant="extended"
                                    >
                                        <AddIcon /> Upload photo
                                    </Fab>

                                </label>
                            </div>
                            {/* <Upload></Upload> */}
                        </Grid>
                        <Grid item xs={6}></Grid>
                        <Grid item xs={2}>
                            <Save_Image></Save_Image>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={4}>
                    <Grid container spacing={1}>
                        <Grid item xs={4}>
                            <Upload></Upload>
                        </Grid>
                        <Grid item xs={6}></Grid>
                        <Grid item xs={2}>
                            <Save_Image></Save_Image>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={2}></Grid>
            </Grid>
            <img
                width="30%"
                src={file}
            />
        </div>
    );
}
