import React, { useState } from "react";
import ReactDOM from "react-dom";
import AddIcon from "@material-ui/icons/Add";
import { Fab, Button } from "@material-ui/core";


export default function App() {

  const [file, setFile] = useState()

  const [nameFile, setNameFile] = useState('')

  const cargarImagen = (imagen) => {
    setFile(imagen)
  }

  return (
    <div className="App">
      <label htmlFor="upload-photo">
        <input
          style={{ display: "none" }}
          id="upload-photo"
          name="upload-photo"
          type="file"
          onChange={e => { cargarImagen(e.target.files[0]) }}
        />
        <Fab
          color="secondary"
          size="small"
          component="span"
          aria-label="add"
          variant="extended"
        >
          <AddIcon /> Upload photo
        </Fab>

      </label>
      <img
        width="100%"
        src={file}
      />
    </div>
  );
}
// const rootElement = document.getElementById("root");
// ReactDOM.render(<App />, rootElement);
