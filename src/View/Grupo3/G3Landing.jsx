import React from "react";
import "../../css/Grupo3/G3Landing.css";
import NavBar from "../../Component/Grupo3/NavBar3";
import { Route, Switch } from "react-router-dom";
import Login from "./Login";
import VistaPrincipal from "./Vistas1/VistaPrincipal";
import VistaVerTalleres from "./Vistas1/VistaVerTalleres";
import VistaAsistencia from "./Vistas3/VistaAsistencia";
import VistaAdministracion from "./Vistas3/VistaAdministracion";
import VistaFotos from "./Vistas1/VistaFotos";

function G3Landing() {
  return (
    <div>
      <NavBar className="paper" />
      <Switch>
        {/* Routing Grupo 3	*/}
        <Route exact path="/Grupo3/VistaPrincipal" component={VistaPrincipal} />
        <Route exact path="/Grupo3/VistaPrincipal/VistaVerTalleres" component={VistaVerTalleres} />
        <Route exact path="/Grupo3/VistaAsistencia" component={VistaAsistencia} />
        <Route exact path="/Grupo3/VistaAdministracion" component={VistaAdministracion} />
        <Route exact path="/Grupo3/Login" component={Login} />
        <Route exact path="/Grupo3/VistaFotos" component={VistaFotos}/>

        {/* Fin Routing Grupo 3*/}
      </Switch>
    </div>
  );
}

export default G3Landing;
