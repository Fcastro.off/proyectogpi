import React from "react";
import "../../../css/Grupo3/G3Landing.css";
import ComponenteAdminstracion from "../../../Component/Grupo3/componenteAdministracion";

function VistaAdministracion() {
  return (
    <div className="wrapper">
      <ComponenteAdminstracion />
    </div>
  );
}

export default VistaAdministracion;
