import React from "react";
import "../../../css/Grupo3/G3Landing.css";
import ComponenteAsistencia from "../../../Component/Grupo3/componenteAsistencia";

function VistaAsistencia() {
  return (
    <div className="wrapper">
      <ComponenteAsistencia />
    </div>
  );
}

export default VistaAsistencia;
