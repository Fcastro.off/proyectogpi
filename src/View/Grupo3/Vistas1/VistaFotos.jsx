import React from "react";
import "../../../css/Grupo3/G3Landing.css";
import ComponenteFotos from "../../../Component/Grupo3/componenteFotos";

function VistaFotos() {
  return (
    <div className="wrapper">
      <ComponenteFotos />
    </div>
  );
}

export default VistaFotos;
