import React from "react";
import "../../../css/Grupo3/G3Landing.css";
import ComponenteVerTalleres from "../../../Component/Grupo3/componenteVerTalleres";

function VistaVerTalleres() {
  return (
    <div className="wrapper">
      <ComponenteVerTalleres />
    </div>
  );
}

export default VistaVerTalleres;
